#!/usr/bin/env python
from __future__ import print_function
"""
This module contains a decorator for caching an arbitrary
function. The cache is saved persistently stored. The user has to make
sure that the args and kwargs for the function yield a unique return
value. Only functions where all arguments are hashable are
accepted. Here "hashable" means int, float and str. We don't use
collections.Hashable, since this also works with instances of objects
that create a different hash when executing the program again
(recreating the object), but would still yield the same return value
when passed to the function. Lists and dictionaries of "hashable"
objects (also nested) can be used when activating the "useJSON"
option. If you really want to use functions with non-hashable
arguments and know what you are doing you can specify args and kwargs
that should be ignored using the "ignoreArgs" and "ignoreKwargs"
options. In this case the other function arguments need to ensure
unique return values in the context you want that.

The cache is loaded with the first
function call and saved after termination of the program (atexit).

See the "__main__" part for examples of usage. Once meme is in your
PYTHONPATH you can execute the examples by running:

python -m meme.meme

"""

import json
import atexit
import hashlib
from functools import wraps
import os
import time
import types

try:
    import cPickle as pickle
except ImportError:
    import pickle

from .logger import logger

# in python3 'str' is also for unicode
# in python2 you need 'basestring'
try:
    basestring
except NameError:
    basestring = str

#
# Module level options
#
class options(object):

    # refresh the caches for all function calls
    refreshCache = False

    # save cache after at each function call
    alwaysSaveCache = False

    # (re-)load cache at each function call
    alwaysLoadCache = False

    # deactivate memoization?
    deactivated = False

    # make a sha1 out of the hash? (hash will be shorter)
    doSHA1 = True

    # override cache folder location for all future calls where the
    # cache is initialised
    overrideCache = None


#
# Use this to set the options
#
def setOptions(**kwargs):
    for k, v in kwargs.items():
        if not k in dir(options):
            logger.error("option {} does not exist".format(k))
            raise KeyError
        setattr(options, k, v)

#
# Use this to query the options
#
def getOption(option):
    if not option in dir(options):
        logger.error("option {} does not exist".format(k))
        raise KeyError
    getattr(options, option)

#
# The decorator
#
class cache(object):

    def __init__(self, fun=None, **kwargs):
        """
        fun: For using as a decorator - without the brackets (instead of decorator function)
        kwargs:
          useJSON: use the json module to hash the functions arguments
          cacheName: use this name instead of the functions name for storing the cache
          ignoreArgs: ignore these arguments (list of indices) when creating the hash
          ignoreKwargs: ignore these keyword arguments (list of dictionary keys) when creating the hash
          ttl: time to live (in seconds) for cached return values
          checkDependencies: check if args or kwargs values (or listitems of those) are paths
                             - in this case refresh the cache if the file has changed (default: True)
          argHashFunctions: list of functions to apply on custom objects in the args to retrieve a uniqe hash
        """
        self.cache = {}
        self.cachefile = None
        self.fun = fun # needed in case of usage as decorator without kwargs
        self.useJSON = kwargs.get("useJSON", False)
        self.cachedir = kwargs.get("cacheDir", "_cache")
        self.cacheName = kwargs.get("cacheName", None)
        self.ignoreArgs = kwargs.get("ignoreArgs", [])
        self.ignoreKwargs = kwargs.get("ignoreKwargs", [])
        self.ttl = kwargs.get("ttl", None)
        self.checkDependencies = kwargs.get("checkDependencies", True)
        self.argHashFunctions = kwargs.get("argHashFunctions", None)
        self.kwargHashFunctions = kwargs.get("kwargHashFunctions", None)
        atexit.register(self.saveCache)

        # define the list of special kwargs, to be used only for meme
        # they can be given to the decorated function, no matter if
        # the original function takes kwargs or not
        self.specialKwargs = [
            "refreshCache",
            "alwaysLoadCache",
            "alwaysSaveCache",
        ]


    def initCache(self, fun, reloadCache=False):
        "Load cache into memory and/or create it"
        if not options.overrideCache is None:
            if not self.cachedir == options.overrideCache:
                reloadCache = True
            self.cachedir = options.overrideCache
        if self.cache and not reloadCache:
            return
        if not self.cacheName:
            try:
                self.cacheName = fun.__name__
            except AttributeError:
                self.cacheName = fun.__func__.__name__ # for static methods
        self.cachefile = os.path.join(self.cachedir, self.cacheName)
        if not os.path.exists(self.cachedir):
            os.mkdir(self.cachedir)
        if os.path.exists(self.cachefile):
            logger.info("loading cachefile {}".format(self.cachefile))
            with open(self.cachefile, "rb") as _cachefile:
                self.cache = pickle.load(_cachefile)
        else:
            logger.debug("No cachefile found - resetting cache")
            self.cache = {}


    def saveCache(self):
        "Save cache to disk"
        if not self.cache:
            return
        logger.info("saving cachefile {}".format(self.cachefile))
        with open(self.cachefile, "wb") as _cachefile:
            pickle.dump(self.cache, _cachefile)


    def getCleanedArgs(self, *args, **kwargs):
        args = list(args)
        for i in sorted(self.ignoreArgs, reverse=True):
            logger.debug("Removing arg number {}".format(i))
            try:
                args.pop(i)
            except IndexError:
                logger.debug("No arg number {} found - nothing to ignore".format(i))
        for kwarg in self.ignoreKwargs:
            logger.debug("Removing kwarg {}".format(kwarg))
            try:
                kwargs.pop(kwarg)
            except KeyError:
                logger.debug("Kwarg '{}' not found - nothing to ignore".format(kwarg))
        if self.argHashFunctions:
            for arg in args:
                try:
                    i = args.index(arg)
                    if self.argHashFunctions[i]:
                        args[i] = self.argHashFunctions[args.index(arg)](arg)
                except IndexError:
                    logger.debug("No hash function found for arg number "+args.index(arg))
        if self.kwargHashFunctions:
            for kwarg, val in kwargs.items():
                if kwarg in self.kwargHashFunctions:
                    kwargs[kwarg] = self.kwargHashFunctions[kwarg](val)
        logger.debug("Cleaned args, kwargs = {}, {}".format(args, kwargs))
        return args, kwargs


    def getHash(self, *args, **kwargs):
        """
        Returns a hash of the given args and kwargs.
        Raises TypeError if this is not possible.
        """
        args, kwargs = self.getCleanedArgs(*args, **kwargs)
        if not self.useJSON:
            key = ""
            for arg in list(args)+list(kwargs.keys())+list(kwargs.values()):
                if not isinstance(arg, (int, float, basestring)):
                    logger.error("Argument {} not hashable".format(arg))
                    logger.error("Try useJSON=True if the arg is a list or dictionary")
                    raise TypeError
                key += str(arg)
        else:
            logger.debug("Using json.dumps to serialise args and kwargs")
            key = json.dumps(args, sort_keys=True) + json.dumps(kwargs, sort_keys=True)
        logger.debug("Key to be hashed: {}".format(key))
        # hashing here probably only nescessary if each function call
        # is saved as a file (currently not done)
        if options.doSHA1:
            keyhash = hashlib.sha1(key.encode()).hexdigest()
            logger.debug("Hash: {}".format(keyhash))
        else:
            keyhash = key
        return key, keyhash

    def getDependencies(self, *args, **kwargs):
        """
        Checks the supplied arguments and returns a list of (existing) paths
        """
        args, kwargs = self.getCleanedArgs(*args, **kwargs)
        dependencies = []
        for arg in args:
            logger.debug("Checking {}".format(arg))
            if isinstance(arg, basestring):
                if os.path.exists(arg):
                    dependencies.append(arg)
            else:
                try:
                    logger.debug("Trying to loop over {}".format(arg))
                    for item in arg:
                        logger.debug("Checking {}".format(item))
                        if isinstance(item, basestring) and os.path.exists(item):
                            dependencies.append(item)
                except TypeError:
                    pass

        for kwarg, val in kwargs.items():
            logger.debug("Checking {}".format(kwarg))
            if isinstance(val, basestring):
                if os.path.exists(val):
                    dependencies.append(val)
            else:
                try:
                    logger.debug("Trying to loop over {}".format(val))
                    for item in val:
                        logger.debug("Checking {}".format(item))
                        if isinstance(item, basestring) and os.path.exists(item):
                            dependencies.append(item)
                except TypeError:
                    pass
        return dependencies


    def outdatedCache(self, info):
        if self.ttl:
            if info["time"] < time.time()-self.ttl:
                logger.debug("Cache expired for {}".format(self.cacheName))
                logger.debug("Creation time: {}".format(time.strftime("%d.%m.%Y - %H:%M:%S", time.localtime(info["time"]))))
                logger.debug("Expiry date: {}".format(time.strftime("%d.%m.%Y - %H:%M:%S", time.localtime(info["time"]+self.ttl))))
                return True
        return False


    def outdatedDependencies(self, info, *args, **kwargs):
        logger.debug("Checking dependencies")
        if self.checkDependencies:
            for dependency in self.getDependencies(*args, **kwargs):
                if os.path.getmtime(dependency) > info['time']:
                    logger.debug("refreshing cache since {} is younger ({}) than cache ({})".format(dependency, os.path.getmtime(dependency), info['time']))
                    return True
                else:
                    logger.debug("not refreshing cache since {} is older ({}) than cache ({})".format(dependency, os.path.getmtime(dependency), info['time']))
        return False


    def cacheShouldBeLoaded(self, **kwargs):
        if options.refreshCache:
            logger.debug("refreshCache set by options")
            return False
        if kwargs.get("refreshCache", False):
            logger.debug("refreshCache set by kwargs")
            return False
        logger.debug("Cache should be loaded")
        return True


    def cacheShouldBeRefreshed(self, info, *args, **kwargs):
        if self.outdatedCache(info):
            return True
        if self.outdatedDependencies(info, *args, **kwargs):
            return True
        return False


    def __call__(self, *args, **kwargs):
        """
        Returns the decorated function or executes it if the function was
        already given in the constructor (to enable use as decorator
        without kwargs)
        """
        if callable(self.fun):
            f = self.fun
        else:
            f = args[0]
        try:
            if f.__code__.co_varnames[0] == "self" and not 0 in self.ignoreArgs:
                logger.warn("Function {} has a first argument called \"self\"".format(f.__name__))
                logger.warn("If this is a class method you have to ignore the first argument using")
                logger.warn("@cache(ignoreArgs=[0])")
        except AttributeError: # for static methods
            pass
        @wraps(f)
        def wrapper(*args, **kwargs):
            "Decorated function"
            if options.deactivated:
                return f(*args, **kwargs)
            self.ignoreKwargs = set(self.ignoreKwargs)
            for kwarg in self.specialKwargs:
                self.ignoreKwargs.add(kwarg)
            reloadCache = (kwargs.get("alwaysLoadCache", False) or options.alwaysLoadCache)
            self.initCache(f, reloadCache)
            alwaysSaveCache = options.alwaysSaveCache or kwargs.get("alwaysSaveCache", False)
            key, keyhash = self.getHash(*args, **kwargs)
            logger.debug("hash in cache? {}".format(keyhash in self.cache))
            refreshCache = False
            cacheLoaded = False
            if keyhash in self.cache and self.cacheShouldBeLoaded(**kwargs):
                logger.debug("Found cached object!")
                try:
                    ret, info = pickle.loads(self.cache[keyhash])
                    cacheLoaded = True
                except TypeError:
                    logger.error("Can't load cached object")
                    logger.error("Maybe the cache format changed")
                    logger.error("Try to delete the cache folder")
                    raise Exception
                refreshCache = self.cacheShouldBeRefreshed(info, *args, **kwargs)
                logger.debug("refreshCache? {}".format(refreshCache))
            if not cacheLoaded or refreshCache:
                for kwarg in self.specialKwargs:
                    try:
                        kwargs.pop(kwarg)
                        logger.debug("Removing special kwarg {} before calling originial function".format(kwarg))
                    except KeyError:
                        pass
                logger.debug("Now call the function {}({}, {})".format(self.cacheName, args, kwargs))
                ret = f(*args, **kwargs)
                info = {"time" : time.time()}
                self.cache[keyhash] = pickle.dumps((ret, info))
            if alwaysSaveCache:
                self.saveCache()
            logger.debug("Returning {}".format(ret))
            return ret

        # attach self to wrapper function for debugging/testing
        wrapper.decobj = self
        self.decobj = self

        if callable(self.fun):
            return wrapper(*args, **kwargs)
        else:
            return wrapper

        def __get__(self, obj, objtype):
            "TODO: look up if this makes sense"
            self.ignoreArgs = [0] # probably won't work
            return self.__call__

if __name__ == "__main__":
    pass

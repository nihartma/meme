import unittest
import os
import shutil
import sys
import time
import argparse

from .logger import logger
import logging
from .meme import cache, setOptions, getOption

logger.setLevel(logging.WARNING)

class info:
    "global vars for testing"
    memoized = True

@cache
def testFunction(x, **kwargs):
    info.memoized = False
    return x*2

class hans:
    "Arbitrary class"
    pass

class Test(unittest.TestCase):

    @staticmethod
    def delete_cache(f):
        """Helper to delete cache for a decorated function"""
        if f.decobj.cachefile:
            cachefile = f.decobj.cachefile
        else:
            cachefile = os.path.join(f.decobj.cachedir, f.decobj.cacheName)
        logger.info("Deleting {} if it exists".format(cachefile))
        if os.path.exists(cachefile):
            os.remove(cachefile)


    def assertMemoized(self, f, *args, **kwargs):
        """Helper to check if function body was *not* executed (assuming it
        would have set the info.memoized to False
        """
        info.memoized = True
        ret = f(*args, **kwargs)
        self.assertTrue(info.memoized)
        return ret


    def assertNotMemoized(self, f, *args, **kwargs):
        """Helper to check if function body was executed (assuming it sets the
        info.memoized to False
        """
        info.memoized = True
        ret = f(*args, **kwargs)
        self.assertFalse(info.memoized)
        return ret


    def test_memoized(self):
        """Test if function is memoized, also when recreating the decorator
        (across "sessions")"""

        def testFunction(x):
            info.memoized = False
            return x*2

        # create decorated function once to create cache
        # and save it
        memFunction = cache(testFunction)
        self.assertEqual(memFunction(3), 6)
        memFunction.decobj.saveCache()

        # new decorated functon should read the cache and be memoized
        memFunction = cache(testFunction)
        self.assertEqual(self.assertMemoized(memFunction, 3), 6)

        # when deleting cache before, it should not be memoized
        self.delete_cache(memFunction)
        memFunction = cache(testFunction)
        self.assertEqual(self.assertNotMemoized(memFunction, 3), 6)


    def test_basic(self):
        "Test memoization during one session"
        self.assertEqual(testFunction(3), 6)
        self.assertEqual(self.assertMemoized(testFunction, 3), 6)


    def test_different_arg(self):
        "Test if calling with different argument isn't memoized"

        @cache(cachedir="_cache", cacheName="testArg")
        def testArg(x):
            info.memoized = False
            return x*2

        self.delete_cache(testArg)
        self.assertEqual(testArg(5), 10)
        self.assertEqual(self.assertNotMemoized(testArg, 3), 6)

    def test_refresh_kwarg(self):
        "Test refresh flag via kwargs"
        testFunction(5)
        self.assertNotMemoized(testFunction, 5, refreshCache=True)
        self.assertMemoized(testFunction, 5, refreshCache=False)

    def test_refresh_module(self):
        "Test refresh flag via module options"
        setOptions(refreshCache=True)
        testFunction(5)
        self.assertNotMemoized(testFunction, 5)
        setOptions(refreshCache=False)
        self.assertMemoized(testFunction, 5)


    def test_mutable(self):
        """Test if mutable objects are correctly memoized (not modified in the
        cache)"""

        @cache
        def mutableReturner(x):
            return [x]

        a = mutableReturner(5)
        self.assertEqual(a[0], 5)
        a[0] = 27
        self.assertEqual(mutableReturner(5)[0], 5)


    def test_ignoreArgs(self):
        """Test ignoring args and/or kwargs which should not be considered

        when creating the hash"""

        @cache(ignoreArgs=[0], ignoreKwargs=["optionalHans"])
        def functionWithArgsToIgnore(aHans, n, **kwargs):
            info.memoized = False
            return n*n

        self.assertEqual(functionWithArgsToIgnore(hans(), 5), 25)
        self.assertEqual(self.assertMemoized(functionWithArgsToIgnore, hans(), 5), 25)
        self.assertEqual(functionWithArgsToIgnore(hans(), 5, optionalHans=hans()), 25)
        self.assertEqual(self.assertMemoized(functionWithArgsToIgnore, hans(), 5, optionalHans=hans()), 25)


    def test_ignoreMultipleArgs(self):
        """
        Test for ignoring multiple args (see #1)
        """

        @cache(ignoreArgs=[0,1,2])
        def f(a,b,c,d):
            info.memoized = False
            return d

        self.assertEqual(f([],[],[],3), 3)
        self.assertEqual(self.assertMemoized(f, 1, 2, 3, 3), 3)


    def test_hashable(self):
        "Test if non-hashable args are correctly detected"

        @cache
        def functionWithNonHashableArgs(aHans, n):
            return n*n

        with self.assertRaises(TypeError):
            functionWithNonHashableArgs(hans(), 5)


    def test_ttl(self):
        "Test time-to-live functionality"

        @cache(ttl=0.5)
        def expiringCache(x):
            info.memoized = False
            return x

        expiringCache(5)
        self.assertMemoized(expiringCache, 5)
        time.sleep(1)
        self.assertNotMemoized(expiringCache, 5)


    def test_reload_options(self):
        "Test options for reloading/saving cache after each call"

        def returnMe(x, **kwargs):
            info.memoized = False
            return x

        logger.info("Testing alwaysSaveCache as kwarg")
        memFunction = cache(returnMe)
        memFunction(5)
        self.delete_cache(memFunction)
        memFunction(5, alwaysSaveCache=True)
        memFunction = cache(returnMe)
        self.assertMemoized(memFunction, 5)
        logger.info("Testing alwaysSaveCache as module option")
        self.delete_cache(memFunction)
        setOptions(alwaysSaveCache=True)
        memFunction(5)
        memFunction = cache(returnMe)
        self.assertMemoized(memFunction, 5)
        setOptions(alwaysSaveCache=False)
        logger.info("Testing alwaysLoadCache")
        memFunction(5)
        self.delete_cache(memFunction)
        setOptions(alwaysLoadCache=True)
        self.assertNotMemoized(memFunction, 5)
        setOptions(alwaysLoadCache=False)

    def test_dependencies(self):
        """Test dependency functionality (refresh cache if function depends on
        a file with newer mtime than the cache)"""

        @cache()
        def dependsOnFiles(*paths):
            for path in paths:
                with open(path) as f:
                    return f.read().strip()

        @cache(useJSON=True)
        def dependsOnFileList(paths):
            for path in paths:
                with open(path) as f:
                    return f.read().strip()

        def callSwitch(path, case):
            if case == 'args':
                return dependsOnFiles(path)
            elif case == 'list':
                return dependsOnFileList([path])

        for case in ['args', 'list']:
            testPath = 'test_dependency.tmp'

            with open(testPath, 'w') as f:
                f.write('something')
            logger.info("read from file")
            self.assertEqual(callSwitch(testPath, case), 'something')

            logger.info("read again from file")
            self.assertEqual(callSwitch(testPath, case), 'something')

            logger.info("wait a sec and update file")
            time.sleep(1)
            with open(testPath, 'w') as f:
                f.write('updated')
            logger.info("read again from updated file")
            ret = callSwitch(testPath, case)
            self.assertEqual(ret, 'updated')
            os.unlink(testPath)


    def test_deactivate(self):
        "Test if deactivating memoize is working"
        testFunction(5)
        setOptions(deactivated=True)
        logger.info("Calling with deactivated meme")
        self.assertNotMemoized(testFunction, 5)
        setOptions(deactivated=False)
        logger.info("Calling meme activated again")
        self.assertMemoized(testFunction, 5)


    def test_class(self):
        "Test if meme works with bound methods and static methods and class methods"
        class testClass:
            @cache(ignoreArgs=[0])
            def memberFunction(self, x):
                info.memoized = False
                return x

            # this order matters!
            @staticmethod
            @cache
            def staticMethod(x):
                info.memoized = False
                return x

            x = 5
            # this order matters!
            # @classmethod
            # @cache(ignoreArgs=[0])
            @classmethod
            @cache(ignoreArgs=[0])
            def classMethod(cls, **kwargs):
                info.memoized = False
                return kwargs.get("x", cls.x)

        logger.info("Calling decorated bound method")
        testClass().memberFunction(5)
        self.assertEqual(self.assertMemoized(testClass().memberFunction, 5), 5)

        logger.info("Calling static method")
        testClass.staticMethod(5)
        self.assertEqual(self.assertMemoized(testClass.staticMethod, 5), 5)
        self.assertEqual(self.assertMemoized(testClass().staticMethod, 5), 5)

        logger.info("Calling class method")
        testClass.classMethod(x=5)
        self.assertEqual(self.assertMemoized(testClass.classMethod, x=5), 5)
        self.assertEqual(self.assertMemoized(testClass().classMethod, x=5), 5)


    def test_custom_hash(self):

        class Hans:
            x = 5
            y = 4
            def __str__(self):
                return str(self.x)+str(self.y)

        @cache(argHashFunctions=[lambda x : str([x.x,x.y]), None])
        def hanser(hans, k):
            info.memoized = False
            return hans.x+hans.y+k

        self.assertEqual(hanser(Hans(), 2), 11)
        self.assertEqual(self.assertMemoized(hanser, Hans(), 2), 11)
        self.delete_cache(hanser)

        @cache(argHashFunctions=[str, None])
        def hanser(hans, k):
            info.memoized = False
            return hans.x+hans.y+k

        self.assertEqual(self.assertNotMemoized(hanser, Hans(), 2, alwaysLoadCache=True), 11)
        self.assertEqual(self.assertMemoized(hanser, Hans(), 2), 11)
        self.delete_cache(hanser)

        @cache(kwargHashFunctions={"hans" : lambda x : str([x.x,x.y])})
        def hanser(**kwargs):
            hans = kwargs["hans"]
            info.memoized = False
            return hans.x+hans.y

        self.assertEqual(self.assertNotMemoized(hanser, hans=Hans(), alwaysLoadCache=True), 9)
        self.assertEqual(self.assertMemoized(hanser, hans=Hans()), 9)
        self.delete_cache(hanser)


    def test_sha1(self):
        default = getOption("doSHA1")
        setOptions(doSHA1=True)
        testFunction(5)
        self.assertMemoized(testFunction, 5)
        setOptions(doSHA1=default)


    def test_nosha1(self):
        default = getOption("doSHA1")
        setOptions(doSHA1=False)
        testFunction(5)
        self.assertMemoized(testFunction, 5)
        setOptions(doSHA1=default)

class TestOverride(Test):

    """Run all the basic tests, but with override cache directory"""

    @classmethod
    def setUpClass(cls):
        logger.info("Overriding cache directory")
        if os.path.exists("_testCache"):
            logger.info("Deleting _testCache")
            shutil.rmtree("_testCache")
        setOptions(overrideCache="_testCache")
        logger.info("Now run the same tests with _testCache as a directory")

    @classmethod
    def tearDownClass(cls):
        logger.info("Resetting overrideCache back to None")
        setOptions(overrideCache=None)

if __name__ == "__main__":

    # Not sure if this is the best way of introducing custom options
    parser = argparse.ArgumentParser(description='run unit tests for meme')
    parser.add_argument('tests', nargs="*", help='a list of any number of test modules, classes and test methods.')
    parser.add_argument('--debug', action='store_true', default=False, help='print debug messages')
    parser.add_argument('--info', action='store_true', default=False, help='print info messages')
    parser.add_argument('-v', action='store_true', default=False, help='set unittest to verbose')
    args = parser.parse_args()

    if args.debug:
        logging.getLogger("meme").setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)

    if args.info:
        logging.getLogger("meme").setLevel(logging.INFO)
        logger.setLevel(logging.INFO)

    unittest_argv = sys.argv[:1]
    if args.tests:
        unittest_argv += args.tests

    if args.v:
        verbosity = 2
    else:
        verbosity = 1

    unittest.main(argv=unittest_argv, verbosity=verbosity)
